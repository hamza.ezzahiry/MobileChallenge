# MobileChallenge
Mobile Challenge technique

<img src="https://media-exp1.licdn.com/dms/image/C4E0BAQFGsAXVhzEXNA/company-logo_200_200/0/1529316438280?e=1663804800&v=beta&t=4rRy2IqqjOzgIXFKfCbbHCCN6HMe878iR57GHYA9Yvc" width="150">

# App Demo 

[![Demo App](https://yt-embed.herokuapp.com/embed?v=BmST84_8tKQ)](https://www.youtube.com/watch?v=BmST84_8tKQ "Demo App")


# ✔️ Tasks

### 1 - Onboarding

The first phase of the mobile application to be developed consists of presenting an onboarding screen to the user in the first use.

- [x] Detects if it is the first use of the application, in which case display the onboarding
- [x] If the user has already consulted the onboarding screens then go directly to the authentication screen

### 2 - Authentication screen

test the following two scenarios:

- [x] Valid authentication with user `muser/mpassw0rd`
- [x]  Show message "Ce compte est bloqué" for user `muser02/mpassword`
- [x] For any other login, display an error message

### 3- News Screens

 This 3rd phase consists of reproducing the screen above (Discover) using the URL below to retrieve a list of news.

- Science: https://inshortsapi.vercel.app/news?category=science
- Business: https://inshortsapi.vercel.app/news?category=business
- Sports: https://inshortsapi.vercel.app/news?category=sports
- Technology: https://inshortsapi.vercel.app/news?category=technology
- Startup: https://inshortsapi.vercel.app/news?category=startup
- Automotive: https://inshortsapi.vercel.app/news?category=automotive


The objective is therefore:
 
- [x] To display a list of news
- [x] Reproduce a UI (without necessarily activating the Search part)
- [x] Have a horizontal navigation by categories: Science, Business, Sports, Technology, Startup and Automotive

## 📝 Tools and library used

 - [Flutter SDK](https://flutter.dev)

## 📸 ScreenShots

#### Screen 1
<img src="https://i.imgur.com/1WZ5Hci.jpeg">


#### Screen 2
<img src="https://i.imgur.com/NLofXj3.jpeg">

## 🧜🏻 Getting Started

Make sure you have [Flutter SDK](https://flutter.dev) installed.

```bash
# Clone the repo
$ git clone https://gitlab.com/hamza.ezzahiry/MobileChallenge.git
$ cd MobileChallenge

# Install dependencies
$ flutter pub get

# Run
$ flutter run
```

Developer tools running on iOS Simulator or Android Emulator.

## 📦 Packages used:

| Packages                |
| ----------------------- |
| [shared_preferences](https://pub.dev/packages/shared_preferences)  |
| [introduction_screen](https://pub.dev/packages/introduction_screen)  | 
| [flutter_svg](https://pub.dev/packages/flutter_svg)  |
| [rflutter_alert](https://pub.dev/packages/rflutter_alert)  |
| [fluttericon](https://pub.dev/packages/fluttericon)  |
| [http](https://pub.dev/packages/http)  |

## 📝 Notes:

- I put the articleView in the 3rd screen (When you click on Profile button).

## 💻 Author

- **Github** : [@SpiderX](https://www.github.com/hamza-ezzahiry)

