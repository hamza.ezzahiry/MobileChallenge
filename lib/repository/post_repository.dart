import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:challenge/models/post.dart';

// Fetch posts from API
Future<List<Post>> fetchPosts(String customUrl) async {
  final String url = customUrl; // use customUrl for each category
  var response = await http.get(Uri.parse(url));

  return (json.decode(response.body)['data'] as List)
      .map((e) => Post.fromJson(e))
      .toList();
}
