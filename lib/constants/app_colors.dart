import 'package:flutter/material.dart';

Color primaryColor = Color(0xFF6D64FE);
Color accentColor = Color(0xFF151f54);
Color secondaryColor = Color(0xffa3adbc);
Color actionColor = Color(0xFF1CD66C);
Color backgroundColor = Color(0xFFFFFFFF);
