import 'package:flutter/material.dart';

import 'package:challenge/models/post.dart';
import 'package:challenge/widgets/PostTile.dart';
import 'package:challenge/repository/post_repository.dart';

class CategoryList extends StatefulWidget {
  final String? url;
  const CategoryList({Key? key, this.url}) : super(key: key);

  @override
  State<CategoryList> createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList> {
  String? customUrl;
  @override
  void initState() {
    super.initState();
    customUrl = widget.url;
  }

  Widget build(BuildContext context) {
    return Container(
      // color top border only
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(color: Colors.grey, width: 1),
        ),
      ),
      // Fetch posts from API and display them
      child: FutureBuilder<List<Post>>(
        future: fetchPosts(customUrl ?? ""),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Post> _posts = snapshot.data as List<Post>;
            return ListView.builder(
              itemCount: _posts.length,
              scrollDirection: Axis.vertical,
              itemBuilder: (context, index) => PostTile(_posts[index]),
            );
          }
          if (snapshot.hasError) return Center(child: Text('Error!'));
          return Center(
              child: CircularProgressIndicator(color: Colors.black87));
        },
      ),
    );
  }
}
