import 'package:flutter/material.dart';

class ViewArticle extends StatelessWidget {
  const ViewArticle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: CustomScrollView(slivers: [
        SliverAppBar(
          backgroundColor: Colors.white,
          expandedHeight: MediaQuery.of(context).size.height * 0.5,
          collapsedHeight: 60,
          pinned: false,
          leading: const Icon(Icons.arrow_back),
          flexibleSpace: FlexibleSpaceBar(
            collapseMode: CollapseMode.pin,
            background: Container(
              width: MediaQuery.of(context).size.width,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage('https://source.unsplash.com/random'),
                  fit: BoxFit.cover,
                ),
              ),
              child: Container(
                color: Colors.black.withOpacity(0.5),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.grey.withOpacity(0.5),
                        ),
                        child: const Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: 13, vertical: 8),
                          child: Text(
                            "Health",
                            style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin:
                            const EdgeInsets.only(top: 6, left: 20, right: 20),
                        child: const Text(
                          'How Meaningful Is Prediabetes for Older Adults?',
                          style: TextStyle(
                            fontSize: 23,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(
                            top: 8, left: 20, right: 20, bottom: 30),
                        child: const Text(
                          'A new study indicates that the condition might be less of a worry than once believed.',
                          style: TextStyle(
                            fontSize: 13,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Container(
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              BorderRadius.vertical(top: Radius.circular(40)),
                        ),
                        height: 30,
                      )
                    ]),
              ),
            ),
          ),
        ),
        SliverToBoxAdapter(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Row(
                children: [
                  Container(
                    padding: const EdgeInsets.only(
                        top: 5, left: 5, bottom: 5, right: 20),
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Row(
                      children: const [
                        CircleAvatar(
                          radius: 13,
                          backgroundImage: NetworkImage(
                              'https://source.unsplash.com/random/40x40'),
                        ),
                        SizedBox(width: 8),
                        Text(
                          "Michael S.",
                          style: TextStyle(fontSize: 13, color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(width: 10),
                  Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 13, vertical: 8),
                    decoration: BoxDecoration(
                      color: Colors.grey.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Row(
                      children: const [
                        Icon(
                          Icons.access_time,
                          color: Colors.grey,
                          size: 20,
                        ),
                        SizedBox(width: 3),
                        Text('2h')
                      ],
                    ),
                  ),
                  const SizedBox(width: 10),
                  Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 13, vertical: 8),
                    decoration: BoxDecoration(
                      color: Colors.grey.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Row(
                      children: const [
                        Icon(
                          Icons.remove_red_eye_outlined,
                          color: Colors.grey,
                          size: 20,
                        ),
                        SizedBox(width: 5),
                        Text('376')
                      ],
                    ),
                  )
                ],
              ),
              const SizedBox(height: 20),
              const Text(
                "Candidate Biden Called Saudi",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 15),
              const Text(
                """Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose. Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose.
Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose. Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose.
Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose. Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose.
Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose. Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose.
Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose. Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose.
Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose. Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose.
Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose. Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose.
Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose. Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose.
Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose. Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose.
Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose. Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose.
Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose. Capture the beauty that catches your eye with a mirrorless camera that you don't want to lose.""",
                style: TextStyle(fontSize: 17, color: Colors.grey),
              ),
            ]),
          ),
        ),
      ]),
    );
  }
}
