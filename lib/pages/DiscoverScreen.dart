import 'package:challenge/pages/CategoryList.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/linecons_icons.dart';

class DiscoverScreen extends StatefulWidget {
  const DiscoverScreen({Key? key}) : super(key: key);

  @override
  State<DiscoverScreen> createState() => _DiscoverScreenState();
}

class _DiscoverScreenState extends State<DiscoverScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 6,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: IconButton(
                icon: Icon(Icons.menu, color: Colors.black, size: 30),
                onPressed: () {},
              ),
            ),
          ),
          toolbarHeight: MediaQuery.of(context).size.height * 0.3,
          flexibleSpace: Container(
            child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20.0, vertical: 0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Discover',
                      style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      'News from all over the world',
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black54,
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(244, 243, 243, 1),
                          borderRadius: BorderRadius.circular(15)),
                      child: TextField(
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            prefixIcon: Icon(
                              Icons.search,
                              color: Colors.grey,
                            ),
                            suffixIcon: Icon(Linecons.params,
                                color: Colors.grey, size: 20),
                            hintText: "Search",
                            hintStyle:
                                TextStyle(color: Colors.grey, fontSize: 15)),
                      ),
                    ),
                  ],
                )),
          ),
          bottom: const TabBar(
            isScrollable: true,
            labelColor: Colors.black87,
            unselectedLabelColor: Colors.black38,
            labelStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            indicatorColor: Colors.black87,
            indicatorSize: TabBarIndicatorSize.label,
            indicatorWeight: 3,
            tabs: [
              Tab(text: "Science"),
              Tab(text: "Business"),
              Tab(text: "Sports"),
              Tab(text: "Technology"),
              Tab(text: "Startup"),
              Tab(text: "Automobile"),
            ],
          ),
        ),
        body: const TabBarView(
          children: [
            CategoryList(
                url: "https://inshortsapi.vercel.app/news?category=science"),
            CategoryList(
                url: "https://inshortsapi.vercel.app/news?category=business"),
            CategoryList(
                url: "https://inshortsapi.vercel.app/news?category=sports"),
            CategoryList(
                url: "https://inshortsapi.vercel.app/news?category=technology"),
            CategoryList(
                url: "https://inshortsapi.vercel.app/news?category=startup"),
            CategoryList(
                url: "https://inshortsapi.vercel.app/news?category=automobile"),
          ],
        ),
      ),
    );
  }
}
