import 'package:challenge/constants/app_colors.dart';
import 'package:challenge/pages/Home.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class SignIn extends StatefulWidget {
  const SignIn({Key? key}) : super(key: key);

  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  bool _isChecked = false;
  TextEditingController _userNameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: backgroundColor,
          elevation: 0,
          // arrow back
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: secondaryColor, size: 20),
              onPressed: () => {}),
        ),
        body: Container(
          color: Colors.white,
          padding: const EdgeInsets.all(20.0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Text(
                    'Sign In',
                    style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        color: accentColor),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                TextField(
                  controller: _userNameController,
                  decoration: InputDecoration(
                    labelText: 'Username',
                    labelStyle: TextStyle(color: secondaryColor, fontSize: 15),
                    focusColor: actionColor,
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: actionColor, width: 2),
                    ),
                  ),
                ),
                SizedBox(height: 20),
                // Password
                TextField(
                  controller: _passwordController,
                  obscureText: true,
                  decoration: InputDecoration(
                    labelText: 'Password',
                    labelStyle: TextStyle(color: secondaryColor, fontSize: 15),
                    focusColor: actionColor,
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: actionColor, width: 2),
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      SizedBox(
                        height: 24.0,
                        width: 24.0,
                        child: Theme(
                          data: ThemeData(
                            unselectedWidgetColor: secondaryColor, // Your color
                          ),
                          child: Checkbox(
                              activeColor: primaryColor,
                              value: _isChecked,
                              onChanged: _handleRememberMeChanged),
                        ),
                      ),
                      SizedBox(width: 10.0),
                      Text(
                        "Remember Me",
                        style: TextStyle(
                          color: secondaryColor,
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                        ),
                      )
                    ]),
                    Text("Forgot Password?",
                        style: TextStyle(
                          color: primaryColor,
                          fontWeight: FontWeight.w600,
                          fontSize: 12,
                        )),
                  ],
                ),
                SizedBox(height: 40),
                // Sign In
                Container(
                  width: double.infinity,
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: primaryColor,
                  ),
                  child: TextButton(
                    child: Text(
                      "LOGIN",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    onPressed: () {
                      if (_userNameController.text == "muser02" &&
                          _passwordController.text == "mpassword")
                        Alert(
                          context: context,
                          type: AlertType.warning,
                          title: "Warning!",
                          desc: "Ce compte est bloqué",
                          buttons: [
                            DialogButton(
                              color: primaryColor,
                              child: Text(
                                "Ok",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 15),
                              ),
                              onPressed: () => Navigator.pop(context),
                              width: 120,
                            )
                          ],
                        ).show();
                      else if (_userNameController.text == "muser" &&
                          _passwordController.text == "mpassw0rd")
                        // push to Home Page
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Home(),
                          ),
                        );
                      else
                        Alert(
                          context: context,
                          type: AlertType.error,
                          title: "Error!",
                          desc: "Vérifier votre identifiant et mot de passe",
                          buttons: [
                            DialogButton(
                              color: primaryColor,
                              child: Text(
                                "Ok",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 15),
                              ),
                              onPressed: () => Navigator.pop(context),
                              width: 120,
                            )
                          ],
                        ).show();
                    },
                  ),
                ),
                SizedBox(height: 20),
                // Have user? sign up
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'New user?',
                      style: TextStyle(
                          color: secondaryColor,
                          fontSize: 12,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(width: 5),
                    Text(
                      'Signup',
                      style: TextStyle(
                          color: primaryColor,
                          fontWeight: FontWeight.w600,
                          fontSize: 12),
                    ),
                  ],
                ),
              ]),
        ));
  }

  void _handleRememberMeChanged(bool? value) {
    setState(() {
      _isChecked = value ?? false;
    });
  }
}
