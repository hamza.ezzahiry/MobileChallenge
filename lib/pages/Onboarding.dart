import 'package:flutter/material.dart';
import 'package:challenge/constants/app_colors.dart';
import 'package:introduction_screen/introduction_screen.dart';

// My own custom widget
import 'package:challenge/pages/SignIn.dart';
import 'package:challenge/widgets/Slide.dart';

class onBoardingPage extends StatelessWidget {
  const onBoardingPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return IntroductionScreen(
      pages: [
        /// Slide 1
        Slide(
          title: "Create an account",
          buttonTitle: "Connect with people\naround the world",
          bodyText:
              "Users will be able to go live, chat and\nmeet people near by",
          pageDec: buildDecoration(),
          imageWidth: MediaQuery.of(context).size.width * 0.8,
          image: "assets/onboarding/onboard1bg.svg",
        ),

        /// Slide 2
        Slide(
          title: "Log in to your account",
          buttonTitle: "Let's build connection with new people",
          bodyText:
              "Users will be able to go live, chat and\nmeet people near by",
          pageDec: buildDecoration(),
          imageWidth: MediaQuery.of(context).size.width * 0.8,
          image: "assets/onboarding/onboard2bg.svg",
        ),

        /// Slide 3
        Slide(
          title: "Log in to your account",
          buttonTitle: "Feel the happiness\n",
          bodyText:
              "Users will be able to go live, chat and\nmeet people near by",
          pageDec: buildDecoration(),
          imageWidth: MediaQuery.of(context).size.width * 0.8,
          image: "assets/onboarding/onboard3bg.svg",
          context: context,
        ),
      ],
      next: Text(
        'NEXT',
        style: TextStyle(color: primaryColor, fontSize: 15),
      ),
      doneSemantic: "GET STARTED",
      showDoneButton: false,
      onDone: () => Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (_) => SignIn()),
      ),
      showSkipButton: true,
      skip: Text(
        'SKIP',
        style: TextStyle(color: secondaryColor, fontSize: 15),
      ), //by default, skip goes to the last page
      onSkip: () => Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (_) => SignIn()),
      ),
      dotsDecorator: DotsDecorator(
        color: backgroundColor,
        activeColor: backgroundColor,
        activeSize: Size(10, 10),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      animationDuration: 1000,
      globalBackgroundColor: backgroundColor,
    );
  }

  DotsDecorator getDotDecoration() => DotsDecorator(
      color: Colors.grey,
      size: Size(5, 5),
      activeColor: primaryColor,
      activeSize: Size(5, 5),
      activeShape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ));

  PageDecoration buildDecoration() => PageDecoration(
        titleTextStyle: TextStyle(
            fontSize: 25, fontWeight: FontWeight.bold, color: primaryColor),
        bodyTextStyle: TextStyle(fontSize: 20),
        pageColor: backgroundColor,
        imagePadding: EdgeInsets.all(0),
      );
}
