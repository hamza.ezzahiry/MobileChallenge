import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  /// I don't have an Data API in this case to fetch, so I made a List of the static posts to display.
  List _elements = [
    {
      'title': "Candidate Biden Called Saudi Arabia a 'Pariah.'",
      'imageurl': 'https://i.imgur.com/nJDuixh.png',
      'time': '4 hours ago',
      'author': 'Davide E Sanger',
    },
    {
      'title': "Rep. Marjorie Taylor Greene faces a backlash ",
      'imageurl': 'https://i.imgur.com/oI82eMB.png',
      'time': '2 hours ago',
      'author': 'Rachedl Levine',
    },
    {
      'title': "Candidate Biden Called Saudi Arabia a 'Pariah.'",
      'imageurl': 'https://i.imgur.com/nJDuixh.png',
      'time': '4 hours ago',
      'author': 'Davide E Sanger',
    },
    {
      'title': "Rep. Marjorie Taylor Greene faces a backlash ",
      'imageurl': 'https://i.imgur.com/oI82eMB.png',
      'time': '2 hours ago',
      'author': 'Rachedl Levine',
    },
    {
      'title': "Candidate Biden Called Saudi Arabia a 'Pariah.'",
      'imageurl': 'https://i.imgur.com/nJDuixh.png',
      'time': '4 hours ago',
      'author': 'Davide E Sanger',
    },
    {
      'title': "Rep. Marjorie Taylor Greene faces a backlash ",
      'imageurl': 'https://i.imgur.com/oI82eMB.png',
      'time': '2 hours ago',
      'author': 'Rachedl Levine',
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: MediaQuery.of(context).size.height * 0.5,
        elevation: 0,
        backgroundColor: Colors.transparent,
        bottomOpacity: 0.0,
        leading: Align(
          alignment: Alignment.topLeft,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: IconButton(
              icon: Icon(Icons.menu, color: Colors.white, size: 30),
              onPressed: () {},
            ),
          ),
        ), // to remove the back button
        flexibleSpace: Container(
          clipBehavior: Clip.hardEdge,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/image.jpg'),
              fit: BoxFit.cover,
            ),
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(25),
            ),
          ),
          child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Colors.black.withOpacity(0.6),
                    Colors.black.withOpacity(0.27),
                    Colors.black.withOpacity(0.3),
                  ],
                ),
              ),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          'News of the day',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 10,
                              fontWeight: FontWeight.w500,
                              color: Colors.white),
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          color: Colors.white.withOpacity(0.2),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "'V.I.P. Immunization’ for the Powerful and Their Cronies Rattles South America",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      // learn more and icon arrow right inside a container
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              'Learn More',
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Icon(
                              Icons.arrow_right_alt_outlined,
                              color: Colors.white,
                              size: 30,
                            ),
                          ],
                        ),
                      ),
                    ]),
              )),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        height: MediaQuery.of(context).size.height * 0.45,
        child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Breaking News',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w900,
                ),
              ),
              Text(
                "More",
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          SizedBox(height: 20),
          Container(
            height: MediaQuery.of(context).size.height * 0.31,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: _elements.length,
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.only(right: 10),
                  padding: EdgeInsets.all(0),
                  width: MediaQuery.of(context).size.width * 0.55,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(20), // Image border
                        child: SizedBox.fromSize(
                          child: Image(
                            image: NetworkImage(
                              _elements[index]['imageurl'],
                            ),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(_elements[index]['title'] ?? '',
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold)),
                      SizedBox(height: 5),
                      Text(
                        _elements[index]['time'] ?? '',
                        style: TextStyle(fontSize: 12, color: Colors.black54),
                      ),
                      SizedBox(height: 5),
                      Text(
                        'By${_elements[index]['author']}',
                        style: TextStyle(fontSize: 12, color: Colors.black54),
                      ),
                    ],
                  ),
                );
              },
            ),
          )
        ]),
      ),
    );
  }
}
