import 'package:challenge/pages/ViewArticle.dart';
import 'package:flutter/material.dart';

import 'package:challenge/pages/HomeScreen.dart';
import 'package:challenge/pages/DiscoverScreen.dart';
// import 'package:challenge/pages/ProfileScreen.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  // index the current page
  int _currentIndex = 0;

  // List of the Screen
  List<Widget> _getScreens() {
    List<Widget> _Screen = <Widget>[
      HomeScreen(),
      DiscoverScreen(),
      // ProfileScreen(),
      ViewArticle(),
    ];
    return (_Screen);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _getScreens().elementAt(_currentIndex),
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,
        showUnselectedLabels: false,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home_outlined),
            activeIcon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search_outlined),
            activeIcon: Icon(Icons.search_rounded),
            label: 'Weather',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_outline),
            activeIcon: Icon(Icons.person_rounded),
            label: 'Profile',
          ),
        ],
        currentIndex: _currentIndex,
        onTap: _changeItem,
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.grey,
      ),
    );
  }

  void _changeItem(int value) // change the current page
  {
    setState(() {
      _currentIndex = value;
    });
  }
}
