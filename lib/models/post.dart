class Post {
  final String? author;
  final String? content;
  final String? date;
  final String? id;
  final String? imageUrl;
  final String? readMoreUrl;
  final String? time;
  final String? title;
  final String? url;

  Post({
    this.author,
    this.content,
    this.date,
    this.id,
    this.imageUrl,
    this.readMoreUrl,
    this.time,
    this.title,
    this.url,
  });

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
        author: json['author'],
        content: json['content'],
        date: json['date'],
        id: json['id'],
        imageUrl: json['imageUrl'],
        readMoreUrl: json['readMoreUrl'],
        time: json['time'],
        title: json['title'],
        url: json['url']);
  }
}
