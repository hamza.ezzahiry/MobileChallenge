import 'package:challenge/pages/SignIn.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:introduction_screen/introduction_screen.dart';

import 'package:challenge/constants/app_colors.dart';

PageViewModel Slide({
  @required String? title,
  @required String? buttonTitle,
  @required String? bodyText,
  @required String? image,
  @required PageDecoration? pageDec,
  @required double? imageWidth,
  BuildContext? context,
}) =>
    PageViewModel(
      useScrollView: false,
      titleWidget: Container(
        alignment: Alignment.topLeft,
        padding: EdgeInsets.fromLTRB(10, 40, 0, 0),
        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(title ?? "", style: TextStyle(fontSize: 15)),
            SizedBox(height: 10),
            Text(buttonTitle ?? "",
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: 34,
                    color: accentColor,
                    fontWeight: FontWeight.bold)),
            // SizedBox(height: 20),
          ],
        ),
      ),
      bodyWidget: Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 30),
            SvgPicture.asset(
              image ?? "",
              width: imageWidth,
              height: 200,
            ),
            SizedBox(height: 20),
            Text(bodyText ?? "",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 15,
                  color: accentColor,
                )),
          ],
        ),
      ),
      decoration: pageDec ??
          PageDecoration(
            titleTextStyle: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: accentColor,
            ),
          ),
      footer: (image == 'assets/onboarding/onboard3bg.svg')
          ? Expanded(
              // heightFactor: 10,
              child: OutlinedButton(
                child: Text(
                  'GET STARTED',
                  style: TextStyle(color: backgroundColor, fontSize: 17),
                ),
                style: OutlinedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  backgroundColor: primaryColor,
                  primary: primaryColor,
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                ),
                onPressed: () => Navigator.of(context!).pushReplacement(
                  MaterialPageRoute(builder: (_) => SignIn()),
                ),
              ),
            )
          : Container(),
    );

DotsDecorator getDotDecoration() => DotsDecorator(
    color: Colors.grey,
    size: Size(5, 5),
    activeColor: primaryColor,
    activeSize: Size(5, 5),
    activeShape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(5),
    ));
