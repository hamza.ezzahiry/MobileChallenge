import 'package:flutter/material.dart';
import 'package:challenge/models/post.dart';

class PostTile extends StatelessWidget {
  final Post _post;
  PostTile(this._post);

  @override
  Widget build(BuildContext context) => Column(
        children: <Widget>[
          ListTile(
            title: Padding(
              padding: const EdgeInsets.only(bottom: 4),
              child: Text(
                _post.title ?? "",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
              ),
            ),
            visualDensity: VisualDensity(vertical: 3),
            subtitle: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Icon(
                      Icons.watch_later_outlined,
                      size: 15,
                    ),
                    SizedBox(width: 5),
                    Text("4 hours ago")
                  ],
                ),
                Row(
                  children: [
                    Icon(
                      Icons.remove_red_eye_outlined,
                      size: 15,
                    ),
                    SizedBox(width: 5),
                    Text(
                        "${_post.title!.length + 399} views") // to make view number look like a real post
                  ],
                ),
              ],
            ),
            leading: Container(
              width: 80.0,
              height: 90.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(_post.imageUrl ?? "")),
                borderRadius: BorderRadius.all(Radius.circular(8.0)),
              ),
            ),
          ),
        ],
      );
}
